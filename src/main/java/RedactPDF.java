package com.gitlab;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.ArgumentParsers;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Properties;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
public final class RedactPDF {

    private RedactPDF() {
    }

    public static void main(String[] args) throws IOException {


        /*
         READ USER INPUT
         */
        Map<String, String> inputMap = processInput(args);
        String inputDir = inputMap.get("inputDir");
        String outputDir = inputMap.get("outputDir");
        String configFile = inputMap.get("configFile");
        /*
        READ REPLACE CONFIG FILE
         */
        Properties toReplace = readPropertiesFile(configFile);

        PDDocument document = null;

        // Create filter for PDF files
        FilenameFilter pdfFilter = new FilenameFilter() {

            public boolean accept(File f, String name)
            {
                return name.endsWith("pdf");
            }
        };
        // Get the list of files in the inputDIr
        File dir = new File(inputDir);
        File[] pdfFiles = dir.listFiles(pdfFilter);
        // Iterate through files in inputDir
        if (pdfFiles != null) {
            for (File f : pdfFiles) {
                PDDocument doc = null;
                // Load the pdf doc
                doc = PDDocument.load(f);
                // Iterate through the toReplace properties & replace.
                System.out.println(f.getName() + " - processing");
                Set<String> keys = toReplace.stringPropertyNames();
                for (String key : keys) {
                    doc = replaceText(doc, key, toReplace.getProperty(key));
                    //System.out.println(key + " : " + toReplace.getProperty(key));
                }
                // Write file to output dir.
                doc.save(outputDir + "/" + f.getName());
                doc.close();
                System.out.println(f.getName() + " - complete");
            }
        }


    }


    /*
        Read input args & return a HashMap
     */
    private static Map<String, String> processInput(String[] args) throws IOException {
        String workingDir = System.getProperty("user.dir") + "/";

        Map <String, String> hm = new HashMap<String, String>();

        ArgumentParser parser = ArgumentParsers.newFor("RedactPDF").build()
                .description("Redact your PDF.");

        String id = "input";
        String od = "output";
        String cf = "config/redact.config";
        parser.addArgument("--id")
                .metavar("path")
                //.required(true)
                .type(String.class)
                .setDefault(workingDir + id)
                .help("The input directory of your PDFs. (default: ./" + id + ")");

        parser.addArgument("--od")
                .metavar("path")
                //.required(true)
                .type(String.class)
                .setDefault(workingDir + od)
                .help("The output directory of your PDFs. (default: ./" + od + ")");

        parser.addArgument("--cf")
                .metavar("path")
                .type(String.class)
                .setDefault(workingDir + cf)
                .help("The replacement key/value config file. (default: ./" + cf + ")");
        try {
            Namespace argns = parser.parseArgs(args);
            hm.put("inputDir", (String) argns.get("id"));
            hm.put("outputDir", (String) argns.get("od"));
            hm.put("configFile", (String) argns.get("cf"));
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }
        return hm;
    }
    /*

     */
    private static Properties readPropertiesFile(String fileName) throws IOException {
        FileInputStream fis = null;
        Properties props = null;
        try {
            fis = new FileInputStream(fileName);
            // create Properties class object
            props = new Properties();
            // load properties file into it
            props.load(fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            fis.close();
        }

        return props;

    }

    private static PDDocument replaceText(PDDocument document, String searchString, String replacement) throws IOException {
        if (StringUtils.isEmpty(searchString)) {
            return document;
        }
        if (StringUtils.isEmpty(replacement)) {
            replacement = "";
        }
        searchString = "^" + searchString + "$";
        for (PDPage page : document.getPages()) {
            PDFStreamParser parser = new PDFStreamParser(page);
            parser.parse();
            List<?> tokens = parser.getTokens();

            for (int j = 0; j < tokens.size(); j++) {
                Object next = tokens.get(j);
                if (next instanceof Operator) {
                    Operator op = (Operator) next;

                    String pstring = "";
                    int prej = 0;

                    if (op.getName().equals("Tj")) {
                        COSString previous = (COSString) tokens.get(j - 1);
                        String string = previous.getString();
                        string = string.replaceFirst(searchString, replacement);
                        previous.setValue(string.getBytes());
                    } else if (op.getName().equals("TJ")) {
                        COSArray previous = (COSArray) tokens.get(j - 1);
                        for (int k = 0; k < previous.size(); k++) {
                            Object arrElement = previous.getObject(k);
                            if (arrElement instanceof COSString) {
                                COSString cosString = (COSString) arrElement;
                                String string = cosString.getString();

                                if (j == prej) {
                                    pstring += string;
                                } else {
                                    prej = j;
                                    pstring = string;
                                }
                            }
                        }

                        if (searchString.equals(pstring.trim())) {
                            COSString cosString2 = (COSString) previous.getObject(0);
                            cosString2.setValue(replacement.getBytes());

                            int total = previous.size() - 1;
                            for (int k = total; k > 0; k--) {
                                previous.remove(k);
                            }
                        }
                    }
                }
            }
            PDStream updatedStream = new PDStream(document);
            OutputStream out = updatedStream.createOutputStream(COSName.FLATE_DECODE);
            ContentStreamWriter tokenWriter = new ContentStreamWriter(out);
            tokenWriter.writeTokens(tokens);
            out.close();
            page.setContents(updatedStream);
        }

        return document;
    }

}