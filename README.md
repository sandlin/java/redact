# Redact
## About
This package iterates through PDF files and attempts to redact specific text. Be aware: PDF is a varying format. Thus, this will not work with all PDF files.

## Arguments
```java
➜ java -jar target/RedactPDF-1.0-SNAPSHOT-jar-with-dependencies.jar --help
usage: RedactPDF [-h] [--id path] [--od path] [--cf path]

Redact your PDF.

named arguments:
  -h, --help             show this help message and exit
  --id path              The input directory of your PDFs. (default: ./input)
  --od path              The output directory of your PDFs. (default: ./output)
  --cf path              The replacement key/value config file. (default: ./config/redact.config)


```


## Usage
1) Create a key / value file in the config dir, defining what strings to replace, and with what values.
   1) This supports blank replacement values.
1) Copy your PDF files into the `input` directory.
1) Execute RedactPDF via `java -jar RedactPDF.jar` with or without args.